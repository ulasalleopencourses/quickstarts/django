



# Django

El objetivo de este proyecto es aprender sobre Django y su uso, aprenderemos sobre los pros y contras del uso de esta herramienta, cómo nos ayudaría en cuanto el desarrollo de un proyecto cualquiera.

## Herramientas:

* Django 3.1
* Python 3 
* Visual Studio Code


## Código:
La carpeta código se encuentra el código desarrollado en los videos
* MyExample -> Pequeña intro a Django, ¿Cómo usarlo?
* PrimerProyecto -> Una ApiRest de una pequeña aplicación de una feria de admisión.

## Videos:


* Video 1: Django, introducción e instalación [link1](https://gitlab.com/ulasalleopencourses/quickstarts/django/-/blob/master/Videos/1-Introducción.mp4 "link1")
    * Instalar Django
    * Aprender la estructura general de Django




* Video 2: Primeros pasos en Django [link2](https://gitlab.com/ulasalleopencourses/quickstarts/django/-/blob/master/Videos/2-Primeros-pasos.mp4 "link2")
    * Nuestro primeria línea de código
    * Aprendiendo sobre librerías en Django


 

* Video 3: Nuestro primer proyecto [link3](https://gitlab.com/ulasalleopencourses/quickstarts/django/-/blob/master/Videos/3-Primer%20Proyecto.mp4 "link3")
    * Crear una aplicación web
    * Interactuar con esta




* Video 4: Potencialidades de Django (Django, el versus) [link4](https://gitlab.com/ulasalleopencourses/quickstarts/django/-/blob/master/Videos/4-Potencialidades%20de%20Django%20(Django,%20el%20versus).mp4 "link4")
    * Investigar sobre Django en el mundo
    * Investigar cuán usado es
    * Algunas curiosidades de Django




## Autor:
Jhonny Frans Gallegos Mendoza
jgallegosm@ulasalle.edu.pe
