from django.contrib import admin

# Register your models here.
from API.models import *


class StandInline(admin.StackedInline):
    model = Stand
    # Mostramos dos inlines acíos por defecto
    extra = 2


class SedeAdmin(admin.ModelAdmin):
    # Registramos el inline en la persona
    inlines = [StandInline]

admin.site.register(Sede, SedeAdmin)
admin.site.register(Stand)
admin.site.register(EgresadosExito)
admin.site.register(OrientacionVocacional)
admin.site.register(ActividadesWebinars)
admin.site.register(ActividadesVideos)
admin.site.register(Participante)
admin.site.register(Pregunta)
admin.site.register(Respuesta)
admin.site.register(RespuestaParticipante)