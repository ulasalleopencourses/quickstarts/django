from rest_framework import serializers
from API.models import Sede


class SedeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sede
        fields = ['nombre']
