from django.db import models


# Create your models here.
class Sede(models.Model):
    nombre = models.CharField(max_length=200)

    

class Stand(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, blank=True, null=True, related_name="stands")
    nombre = models.CharField(max_length=200)
    videoCarrera = models.TextField(blank=True, null=True)
    videoAlumno = models.TextField(blank=True, null=True)
    brochure = models.FileField(blank=True, null=True)
    datosEmpleabilidad = models.TextField(blank=True, null=True)
    imagen1 = models.ImageField(blank=True, null=True)
    imagen2 = models.ImageField(blank=True, null=True)
    imagen3 = models.ImageField(blank=True, null=True)


class EgresadosExito(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, blank=True, null=True, related_name="egresados")
    video = models.TextField(blank=True, null=True)


class OrientacionVocacional(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, blank=True, null=True, related_name="orientacion")
    video = models.TextField(blank=True, null=True)


class ActividadesWebinars(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, blank=True, null=True, related_name="actividades_webinars")
    link_webinar = models.TextField(blank=True, null=True)


class ActividadesVideos(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, blank=True, null=True, related_name="actividades_videos")
    link_webinar = models.TextField(blank=True, null=True)


class Participante(models.Model):
    dni = models.CharField(max_length=200, blank=True, null=True)
    nombres = models.CharField(max_length=200, blank=True, null=True)
    apellidos = models.CharField(max_length=200, blank=True, null=True)
    puntos_ruta_tecsup = models.IntegerField(default=0)

    def getPuntajeTrivia(self):
        totalCorrectas = self.participante_respuestas.filter(respuesta__correcta=True).count()
        return totalCorrectas;


class Pregunta(models.Model):
    pregunta = models.TextField(max_length=200, blank=True, null=True)
    imagen = models.ImageField(blank=True, null=True)


class Respuesta(models.Model):
    pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE, related_name="respuestas")
    respuesta = models.TextField(max_length=200, blank=True, null=True)
    imagen = models.ImageField(blank=True, null=True)
    correcta = models.BooleanField(default=False)


class RespuestaParticipante(models.Model):
    respuesta = models.ForeignKey(Respuesta, on_delete=models.CASCADE,)
    participante = models.ForeignKey(Participante, on_delete=models.CASCADE, related_name="participante_respuestas")