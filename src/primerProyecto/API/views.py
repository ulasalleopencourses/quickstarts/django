from django.http import HttpResponse
from rest_framework import generics
from API.models import Sede

from API.serializers import SedeSerializer

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

class SedeList(generics.CreateAPIView):
    queryset = Sede.objects.all()
    serializer_class = SedeSerializer